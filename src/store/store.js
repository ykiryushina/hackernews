import { configureStore } from '@reduxjs/toolkit';
import { newsSlice } from './news-slice';

export const store = configureStore({
    reducer: { news: newsSlice.reducer }
});