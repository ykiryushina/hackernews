import { newsActions } from "./news-slice";

export const fetchNews = () => {
    return async (dispatch) => {
        const getTopIds = async () => {
            const response = await fetch('https://hacker-news.firebaseio.com/v0/topstories.json');
            let data = await response.json();
            if (!response.ok) {
                throw new Error(data.message);
            }
            return data;
        };

        const getItems = async (id) => {
            const response = await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json`);
            let data = await response.json();
            if (!response.ok) {
                throw new Error(data.message);
            }
            return data;
        }

        try {
            const ids = await getTopIds();
            const topIds = ids.slice(0, 100);
            const items = await Promise.all(topIds.map(async (id) => {
                return await getItems(id);
            }));
            dispatch(newsActions.setItems(items.sort((a, b) => b.time - a.time)));
        } catch (error) {
            return error;
        }
    }
}