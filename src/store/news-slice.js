import { createSlice } from "@reduxjs/toolkit";

export const newsSlice = createSlice({
    name: 'news',
    initialState: {
        items: [],
    },
    reducers: {
        setItems(state, action) {
            state.items = action.payload;
        },

        clearItems(state) {
            state.items = [];
        }
    }
})

export const newsActions = newsSlice.actions;