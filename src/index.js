import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import { store } from './store/store';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { ItemPage } from './pages/ItemPage';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path='/' exact>
          <Redirect to='/items' />
        </Route>
        <Route path='/items' exact>
          <App />
        </Route>
        <Route path='/items/:id'>
          <ItemPage />
        </Route>
      </Switch>
    </BrowserRouter>
  </Provider>
);