import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Item } from './components/Item/Item';
import { fetchNews } from './store/news-actions';
import { Loader } from './components/Loader/Loader';
import { Button } from './components/Button/Button';
import { newsActions } from './store/news-slice';
import './App.css';

function App() {
  const dispatch = useDispatch();
  const items = useSelector(state => state.news.items);

  useEffect(() => {
    dispatch(fetchNews());
    const interval = setInterval(() => {
      dispatch(fetchNews());
    }, 60000);

    return () => {
      clearInterval(interval);
    }
  }, [dispatch]);

  const reloadHandler = () => {
    dispatch(fetchNews());
    dispatch(newsActions.clearItems());
  }

  return (
    <div className="App">
      <div className='centered'>
        {items.length === 0 && <Loader />}
      </div>
      {items.length !== 0 && <Button text={'Reload news'} onClick={reloadHandler} />}
      <ul>
        {items.map((item, i) => (
          <Item
            key={item.id}
            id={item.id}
            title={item.title}
            date={item.time}
            nickname={item.by}
            rate={item.score}
            number={i + 1}
          />
        ))}
      </ul>
    </div>
  );
}

export default App;
