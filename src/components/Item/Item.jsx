import { Link } from 'react-router-dom';
import { Card } from '../Card/Card';
import s from './Item.module.css';

export const Item = ({ number, title, nickname, date, rate, id }) => {
    const newDate = new Date(date * 1000);
    const year = newDate.getFullYear();
    let month = newDate.getMonth();
    let day = newDate.getDay();
    if (day.toString().length === 1) day = `0${day}`;
    if (month.toString().length === 1) month = `0${month}`;
    const convertedDate = `${day}/${month}/${year}`;

    return (
        <Link to={`/items/${id}`} className={s.link}>
            <Card>
                <div className={s.wrapper}>
                    <div className={s.number}>{number}</div>
                    <div className={s.info}>
                        <div className={s.title}>{title}</div>
                        <div className={s.nickname}>by <span>{nickname}</span></div>
                        <div className={s.date}>{convertedDate}</div>
                    </div>
                </div>
                <div className={s['rate-wrapper']}>
                    <p className={s['rate-title']}>Rate:</p>
                    <div className={s.rate}>{rate}</div>
                </div>
            </Card>
        </Link>
    )
}