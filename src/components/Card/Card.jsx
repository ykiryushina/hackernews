import s from './Card.module.css';

export const Card = ({ children }) => {
    return (
        <li className={s.card}>{children}</li>
    )
}