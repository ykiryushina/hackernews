import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

export const ItemPage = () => {
    const params = useParams();
    const items = useSelector(state => state.news.items);
    const chosenItem = items.find(item => item.id === parseInt(params.id));
    console.log(chosenItem);
    return (
        <div>
            Item page
        </div>
    )
}